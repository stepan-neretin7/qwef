import socket
import struct
import threading

from lib import GameMessage


class MulticastReceiver(threading.Thread):
    def __init__(self, listener):
        super().__init__()
        self.listener = listener
        self.socket = None
        self._is_stopping = False

    def run(self):
        multicast_group = '239.192.0.4'
        server_address = ('', 9192)

        try:
            print("Multicast receiver started")
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.socket.bind(server_address)
            group = socket.inet_aton(multicast_group)
            mreq = struct.pack('4sL', group, socket.INADDR_ANY)
            self.socket.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

            while not self._is_interrupted():
                size_data = self.socket.recv(4)
                message_size = int.from_bytes(size_data, byteorder="big")

                # Receive the message using the size information
                received_unknown_message_data, (sender_address, port) = self.socket.recvfrom(message_size)

                msg = GameMessage().parse(data=received_unknown_message_data)

                self.listener.receive_announcement_msg(msg.announcement, sender_address)

        except Exception as e:
            print(f"Error: {e}")
        finally:
            if self.socket is not None:
                group = socket.inet_aton(multicast_group)
                mreq = struct.pack('4sL', group, socket.INADDR_ANY)
                self.socket.setsockopt(socket.IPPROTO_IP, socket.IP_DROP_MEMBERSHIP, mreq)
                self.socket.close()
            print("Multicast receiver finished")

    def _is_interrupted(self):
        return self._is_stopping or self.is_alive()

    def stop(self):
        self._is_stopping = True

    def stopped(self):
        return self._is_stopping

    def stop_thread(self):
        self.stop()
        self.join()
