import sys
from PyQt5.QtWidgets import QApplication
from model.presenter import Presenter
import view.windows.main_window as t


def main():
    app = QApplication(sys.argv)
    view = t.MainGuiWindow()
    presenter = Presenter(view)
    presenter.launch_ui()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
