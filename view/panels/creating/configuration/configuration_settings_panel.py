from PyQt5.QtWidgets import QWidget, QPushButton, QLineEdit, QVBoxLayout, QLabel, QMessageBox
from PyQt5.QtGui import QPixmap, QColor
from PyQt5.QtCore import Qt, QRect


class ConfigurationSettingsPanel(QWidget):
    def __init__(self, listener, width, height):
        super().__init__()

        self.listener = listener
        self.width = width
        self.height = height
        self.setting_width = int(width * 0.08)
        self.setting_height = int(width * 0.03)
        self.font_size = int(width * 0.013)
        self.is_saved = False

        self.width_game = 40
        self.height_game = 30
        self.food_static = 1
        self.food_per_player = 1
        self.state_delay = 1000
        self.dead_food_prob = 0.1
        self.ping_delay = 100
        self.node_timeout = 800

        self.create_ui()

    def create_ui(self):
        self.setStyleSheet("background-image: url(resources/ConfigurationSettings.png);")

        layout = QVBoxLayout(self)

        layout.addWidget(self.create_button("Close", self.listener.close_the_game, 0.01570, 0.0277, 0.0929, 0.1041))
        layout.addWidget(self.create_button("Back", self.listener.back_to_creating_menu, 0.009, 0.16, 0.11, 0.082))
        layout.addWidget(self.create_button("Save", self.save_config, 0.622, 0.77, 0.35, 0.175))
        layout.addWidget(self.create_button("Reset", self.reset_config, 0.52, 0.67, 0.098, 0.086))

        self.width_game_field = self.create_text_field(str(self.width_game), Qt.black, 3, 0.325, 0.295, 0.08, 0.03)
        self.height_game_field = self.create_text_field(str(self.height_game), Qt.black, 3, 0.325, 0.395, 0.08, 0.03)
        self.food_static_field = self.create_text_field(str(self.food_static), Qt.black, 3, 0.795, 0.289, 0.08, 0.03)
        self.food_per_player_field = self.create_text_field(str(self.food_per_player), Qt.black, 3, 0.795, 0.389, 0.08,
                                                            0.03)
        self.state_delay_field = self.create_text_field(str(self.state_delay), Qt.black, 4, 0.325, 0.726, 0.08, 0.03)
        self.dead_food_prob_field = self.create_text_field(str(self.dead_food_prob), Qt.black, 3, 0.795, 0.546, 0.08,
                                                           0.03)
        self.ping_delay_field = self.create_text_field(str(self.ping_delay), Qt.black, 4, 0.325, 0.852, 0.08, 0.03)
        self.node_timeout_field = self.create_text_field(str(self.node_timeout), Qt.black, 4, 0.325, 0.61, 0.08, 0.03)

        layout.addWidget(self.width_game_field)
        layout.addWidget(self.height_game_field)
        layout.addWidget(self.food_static_field)
        layout.addWidget(self.food_per_player_field)
        layout.addWidget(self.state_delay_field)
        layout.addWidget(self.dead_food_prob_field)
        layout.addWidget(self.ping_delay_field)
        layout.addWidget(self.node_timeout_field)

    def create_button(self, text, function, pos_x_ratio, pos_y_ratio, width_ratio, height_ratio):
        button = QPushButton(text, self)
        button.setGeometry(
            int(self.width * pos_x_ratio),
            int(self.height * pos_y_ratio),
            int(self.width * width_ratio),
            int(self.height * height_ratio)
        )
        button.clicked.connect(function)
        return button

    def create_text_field(self, default_text, color, columns, pos_x_ratio, pos_y_ratio, width_ratio, height_ratio):
        text_field = QLineEdit(default_text, self)
        text_field.setStyleSheet(f"color:  blue; font-size: {self.font_size}px;")
        text_field.setMaxLength(columns)
        text_field.setGeometry(
            QRect(
                int(self.width * pos_x_ratio),
                int(self.height * pos_y_ratio),
                int(self.width * width_ratio),
                int(self.height * height_ratio)
            )
        )

        text_field.textChanged.connect(lambda text: self.on_text_changed(text, text_field, default_text))

        return text_field

    def on_text_changed(self, text, text_field, default_text):
        if not text:
            text_field.setText(default_text)
        elif text.isdigit() or text.replace(".", "").isdigit():
            self.update_value(float(text) if "." in text else int(text), text_field)

    def update_value(self, parsed_value, text_field):
        attribute_name = text_field.objectName()
        if not self.is_valid_value(parsed_value, attribute_name):
            text_field.setText(str(getattr(self, attribute_name)))
        else:
            setattr(self, attribute_name, parsed_value)
            print(f"{attribute_name} = {parsed_value}")

    def is_valid_value(self, value, attribute_name):
        limits = {
            "width_game": (10, 100),
            "height_game": (10, 100),
            "food_static": (0, 100),
            "food_per_player": (0, 100),
            "state_delay": (1, 10000),
            "dead_food_prob": (0, 1),
            "ping_delay": (1, 10000),
            "node_timeout": (1, 10000),
        }
        left_limit, right_limit = limits.get(attribute_name, (None, None))
        if left_limit is not None and right_limit is not None:
            if not (left_limit <= value <= right_limit):
                message = f"Please, type value only from interval ({left_limit}, {right_limit})"
                QMessageBox.critical(self, "Invalid Value", message)
                return False
        return True

    def save_config(self):
        self.is_saved = True
        self.listener.back_to_creating_game_menu()

    def reset_config(self):
        self.is_saved = False
        self.width_game = 40
        self.height_game = 30
        self.food_static = 1
        self.food_per_player = 1
        self.state_delay = 1000
        self.dead_food_prob = 0.1
        self.ping_delay = 100
        self.node_timeout = 800

        for field_name in ["width_game", "height_game", "food_static", "food_per_player", "state_delay",
                           "dead_food_prob", "ping_delay", "node_timeout"]:
            field = getattr(self, f"{field_name}_field")
            field.setText(str(getattr(self, field_name)))
