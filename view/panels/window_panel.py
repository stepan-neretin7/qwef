from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap, QImage, QColor
from PyQt5.QtWidgets import QWidget, QLabel, QVBoxLayout


class WindowPanel(QWidget):
    def __init__(self, file_name, width, height):
        super().__init__()

        self.image_label = QLabel(self)
        self.width = width
        self.height = height

        layout = QVBoxLayout(self)
        layout.addWidget(self.image_label)

        self.setFixedSize(width, height)
        self.set_image_icon(file_name)

    def set_image_icon(self, file_name):
        pixmap = QPixmap(file_name)

        if pixmap.isNull():
            default_background = QImage(self.width, self.height, QImage.Format_RGB32)
            default_background.fill(QColor(46, 101, 217))
            self.image_label.setPixmap(QPixmap.fromImage(default_background))
        else:
            pixmap = pixmap.scaled(self.width, self.height, Qt.KeepAspectRatio, Qt.SmoothTransformation)
            self.image_label.setPixmap(pixmap)
