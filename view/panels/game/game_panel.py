from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QGridLayout

from view.panels.game.field_cells_panel import FieldCellsPanel
from view.panels.game.score_line_panel import ScoreLinePanel
from view.utils import get_part
from view.view_utils import ViewUtils


class GamePanel(QWidget):
    def __init__(self, listener, width_panel, height_panel, width_field, height_field):
        super().__init__()

        self.listener = listener
        self.width = width_panel
        self.height = height_panel
        self.field_width = width_field
        self.field_height = height_field
        self.score_line_width = get_part(width_panel, 0.2)
        self.score_line_height = get_part(height_panel, 0.1)
        self.change_role_on_screen = False

        layout = QGridLayout(self)
        self.setLayout(layout)

        self.setFixedSize(width_panel, height_panel)

        cell_size = (int) (self.width * 0.6);
        self.field_cells_panel = FieldCellsPanel(cell_size, self.field_width, self.field_height)
        self.field_cells_panel.setGeometry(0, 0, self.width, self.height)
        self.field_cells_panel.raise_()

        self.node_role = ""
        self.node_role_label = ViewUtils.init_label(
            text=self.node_role,
            font_size=get_part(height_panel, 0.018),
            width=get_part(width_panel, 0.1),
            height=get_part(height_panel, 0.05),
            pos_x=get_part(width_panel, 0.95),
            pos_y=get_part(height_panel, 0.0001)
        )
        self.node_role_label.setParent(self)
        self.node_role_label.raise_()

        button_width = get_part(width_panel, 0.19)
        button_height = get_part(height_panel, 0.1)
        self.change_role_on_viewer = ViewUtils.init_button(
            width=button_width,
            height=button_height,
            pos_x=get_part(width_panel, 0.8),
            pos_y=get_part(height_panel, 0.75),
            listener=self.listener.change_role_on_viewer
        )
        self.leave_the_game = ViewUtils.init_button(
            width=button_width,
            height=button_height,
            pos_x=get_part(width_panel, 0.797),
            pos_y=get_part(height_panel, 0.885),
            listener=self.listener.leave_the_game
        )
        self.leave_the_game.setIcon(ViewUtils.get_image_button_icon(
            file_directory="/BecomeAViewer.png",
            color_for_button=Qt.green,
            width=button_width,
            height=button_height
        ))
        self.change_role_on_screen = False
        self.scroll_pane = ViewUtils.init_scroll_area(
            width=int(self.score_line_width * 1.025),
            height=self.score_line_height * 4,
            pos_x=get_part(width_panel, 0.7),
            pos_y=get_part(height_panel, 0.10)
        )

        self.scroll_pane.setParent(self)
        self.scroll_pane.raise_()

    def update_scores_table(self, scores_table):
        games_panel = QWidget()
        games_panel.setFixedSize(self.score_line_width, self.score_line_height * len(scores_table))
        games_panel.setStyleSheet("background: transparent;")

        layout = QVBoxLayout(games_panel)
        layout.setContentsMargins(0, 0, 0, 0)

        for number, data in enumerate(scores_table):
            score_line_panel = ScoreLinePanel(
                filename="GameLine.png",
                width=self.score_line_width,
                height=self.score_line_height,
                pos_x=0,
                pos_y=self.score_line_height * number,
                data=data
            )
            layout.addWidget(score_line_panel)

        self.scroll_pane.setWidget(games_panel)
        self.scroll_pane.repaint()

    def update_role(self, node_role):
        self.node_role_label.setText(node_role)
        if node_role != "VIEWER":
            if not self.change_role_on_screen:
                self.change_role_on_viewer.setParent(self)
                self.change_role_on_viewer.raise_()
                self.change_role_on_screen = True
        elif self.change_role_on_screen:
            self.change_role_on_viewer.setParent(None)
            self.change_role_on_screen = False

        self.repaint()

    def update_game_panel(self, field, scores_table, node_role):
        if self.node_role != node_role:
            self.node_role = node_role
            self.update_role(node_role)

        self.field_cells_panel.update_field(field)
        self.update_scores_table(scores_table)
