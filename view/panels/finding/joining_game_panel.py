from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QPushButton, QVBoxLayout, QScrollArea

from view.panels.finding.game_line_panel import GameLinePanel
from view.utils import get_part


class JoiningGamePanel(QWidget):
    def __init__(self, listener, width, height):
        super().__init__()

        self.listener = listener
        self.gamePanelsMap = {}
        self.chosenGame = None
        self.gameLineWidth = get_part(width * 1.025, 0.6)
        self.gameLineHeight = get_part(height, 0.1)

        layout = QVBoxLayout(self)

        layout.addWidget(self.create_button("Close", self.listener.close_the_game, get_part(width, 0.0929),
                                            get_part(height, 0.1041), get_part(width, 0.01570),
                                            get_part(height, 0.0277)))
        layout.addWidget(self.create_button("Back", self.listener.back_to_start_menu, get_part(width, 0.11),
                                            get_part(height, 0.082), get_part(width, 0.009), get_part(height, 0.16)))
        layout.addWidget(self.create_button("Join Game", self.join_the_game, get_part(width, 0.093),
                                            get_part(height, 0.15), get_part(width, 0.879), get_part(height, 0.8)))

        self.scrollPane = self.create_scroll_area(self.gameLineWidth, self.gameLineHeight * 7, get_part(width, 0.2),
                                                  get_part(height, 0.25))
        layout.addWidget(self.scrollPane)

    def create_button(self, text, function, pos_x, pos_y, width, height):
        button = QPushButton(text, self)
        button.setGeometry(pos_x, pos_y, width, height)
        button.clicked.connect(function)
        return button

    def create_scroll_area(self, width, height, pos_x, pos_y):
        scroll_area = QScrollArea(self)
        scroll_area.setGeometry(pos_x, pos_y, width, height)
        scroll_area.setWidgetResizable(True)
        scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        content_widget = QWidget()
        scroll_area.setWidget(content_widget)

        inner_layout = QVBoxLayout(content_widget)
        inner_layout.setAlignment(Qt.AlignTop)

        return scroll_area

    def join_the_game(self):
        if self.chosenGame is not None:
            self.listener.joining_to_game(self.chosenGame)

    def choose_game(self, name):
        if self.chosenGame is not None:
            if self.chosenGame == name:
                return
            self.gamePanelsMap[self.chosenGame].make_default_background()
        self.chosenGame = name

    def update_user_list(self, game_list):
        is_chosen_any_dialog = False

        content_widget = self.scrollPane.findChild(QWidget)

        for child in content_widget.findChildren(QWidget):
            child.setParent(None)

        number = 0
        for data in game_list:
            game_line_panel = GameLinePanel(self, "GameLine.png", self.gameLineWidth, self.gameLineHeight, 0,
                                            self.gameLineHeight * number, data)
            if self.chosenGame is None or self.chosenGame == data:
                self.chosenGame = data
                game_line_panel.make_chosen_background()
                is_chosen_any_dialog = True
            self.gamePanelsMap[data] = game_line_panel
            content_widget.layout().addWidget(game_line_panel)
            number += 1

        if not is_chosen_any_dialog and game_list:
            chosen_game = game_list[0]
            self.chosenGame = chosen_game
            self.gamePanelsMap[chosen_game].make_chosen_background()

        self.scrollPane.verticalScrollBar().setValue(0)
