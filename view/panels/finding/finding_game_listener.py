from abc import ABC, abstractmethod


class FindingGameListener(ABC):
    @abstractmethod
    def launch_finding_game_module(self):
        pass

    @abstractmethod
    def close_the_game(self):
        pass

    @abstractmethod
    def back_to_start_menu(self):
        pass
