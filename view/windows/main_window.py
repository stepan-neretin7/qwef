import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QLineEdit, QVBoxLayout, QMessageBox
from PyQt5.QtCore import Qt

from lib import NodeRole
from view.panels.creating.configuration.configuration_settings_panel import ConfigurationSettingsPanel
from view.panels.creating.creating_game_panel import CreatingGamePanel
from view.panels.finding.finding_game_panel import FindingGamePanel
from view.panels.finding.joining_game_panel import JoiningGamePanel
from view.panels.game.game_panel import GamePanel
from view.panels.start_menu.start_panel import StartPanel
from view.view import View


class MainGuiWindow(QMainWindow):
    def __init__(self):

        super().__init__()
        self.NAME = "Snakes"
        self.MENU = "Menu"
        self.widthWindow = 860
        self.heightWindow = self.widthWindow // 16 * 9

        self.widthField = 0
        self.heightField = 0

        self.presenter = None

        self.startPanel = StartPanel(self, self.widthWindow, self.heightWindow)
        self.creatingGamePanel = CreatingGamePanel(self,  self.widthWindow, self.heightWindow)
        self.findingGamePanel = FindingGamePanel(self,  self.widthWindow, self.heightWindow)
        self.joiningGamePanel = JoiningGamePanel(self,  self.widthWindow, self.heightWindow)
        self.configurationSettingsPanel = ConfigurationSettingsPanel(self, self.widthWindow, self.heightWindow)
        self.gamePanel = GamePanel(self, self.widthWindow, self.heightWindow, self.widthField, self.heightField)

        self.setCentralWidget(self.startPanel)
        self.setFocusPolicy(Qt.StrongFocus)
        self.setFixedSize(self.widthWindow, self.heightWindow)

    def set_content_on_frame(self, widget):
        self.setCentralWidget(widget)
        self.repaint()
        self.show()

    def launch_finding_game_module(self):
        self.joiningGamePanel = JoiningGamePanel(self)
        self.set_content_on_frame(self.joiningGamePanel)
        self.presenter.findTheGame(self.findingGamePanel.name, self.findingGamePanel.port,
                                   self.findingGamePanel.player_type)

    def joining_to_game(self, gameKey):
        if self.findingGamePanel.is_viewer:
            self.presenter.joinTheGame(NodeRole.VIEWER, gameKey)
        else:
            self.presenter.joinTheGame(NodeRole.NORMAL, gameKey)

    def open_field(self, widthField, heightField):
        self.gamePanel = GamePanel(self, self.widthWindow, self.heightWindow, widthField, heightField)
        self.set_content_on_frame(self.gamePanel)
        self.widthField = widthField
        self.heightField = heightField

    def close_the_game(self):
        self.presenter.endTheSession()
        sys.exit(0)

    def back_to_creating_menu(self):
        self.set_content_on_frame(self.creatingGamePanel)

    def back_to_start_menu(self):
        self.set_content_on_frame(self.startPanel)

    def create_new_game(self):
        self.creatingGamePanel = CreatingGamePanel(self, self.widthWindow, self.heightWindow)
        self.set_content_on_frame(self.creatingGamePanel)

    def find_games(self):
        self.findingGamePanel = FindingGamePanel(self, self.widthWindow, self.heightWindow)
        self.set_content_on_frame(self.findingGamePanel)

    def attach_presenter(self, presenter):
        self.presenter = presenter

    def change_visible(self, var):
        self.setVisible(var)

    def update_game_view(self, fieldString, scoresTable, nodeRole):
        self.gamePanel.update_game_panel(fieldString, scoresTable, nodeRole)

    def update_find_game_list(self, games):
        self.joiningGamePanel.update_user_list(games)

    def start_the_game(self):
        if self.configurationSettingsPanel.is_saved:
            self.open_field(self.configurationSettingsPanel.width_game, self.configurationSettingsPanel.height_game)
            self.presenter.startTheGame(
                self.creatingGamePanel.name,
                self.creatingGamePanel.port,
                self.creatingGamePanel.player_type,
                self.configurationSettingsPanel.width_game,
                self.configurationSettingsPanel.height_game,
                self.configurationSettingsPanel.food_static,
                self.configurationSettingsPanel.food_per_player,
                self.configurationSettingsPanel.state_delay,
                self.configurationSettingsPanel.dead_food_prob,
                self.configurationSettingsPanel.ping_delay,
                self.configurationSettingsPanel.node_timeout
            )
        else:
            self.widthField = 40
            self.heightField = 30
            self.gamePanel = GamePanel(self, self.widthWindow, self.heightWindow, self.widthField, self.heightField)
            self.presenter.start_the_game(
                self.creatingGamePanel.name,
                self.creatingGamePanel.port,
                self.creatingGamePanel.player_type)

    def open_config_settings(self):
        if self.configurationSettingsPanel is None:
            self.configurationSettingsPanel = ConfigurationSettingsPanel(self, self.widthWindow, self.heightWindow)
        self.set_content_on_frame(self.configurationSettingsPanel)

    def change_role_on_viewer(self):
        self.presenter.changeRoleOnViewer()

    def leave_the_game(self):
        self.back_to_start_menu()
        self.presenter.leaveTheGame()
